# How to set-up multiple servers

Open AccontServer.ini

```
AddTail( -1, 1, "FlyFF", "127.0.0.1", 0, 1, 0, 1 );
	AddTail( 1, 1, "Channel 1", "127.0.0.1", 1, 1, 500, 1 );
```

Look at the two first numbers:
- If the first number is -1, it means it is a server (orange part on [the graph](README.md)).
  - The second number is the Sys id of the server.
- If the first number is not -1, it is the Sys id of the server and this is a channel
  - Second number if the channel id. It must be `> 0` and `< 100`.

The key of a channel is `Sys * 100 + channel id`. All channel keys must be unique.

- IP address of server is the IP address of the LoginServer
  - IP address of the cache is stored in the LoginServer.ini file and sent by Login to Neuz
- IP address of channels is ignored

## Multiple channels

1/ Add a new line for your new channel, for example:

```
	AddTail( 1, 2, "Channel 2", "127.0.0.1", 1, 1, 500, 1 );
```

2/ Edit your `CoreServer.ini` to add all worlds to your new channel.

CoreServer.ini lists the maps for each channel by their key. Just give
all existing maps to each channel.

3/ Copy WorldServer.exe and WorldServer.ini in another folder, modify
the key in the `WorldServer.ini` file.


## Multiple servers

### The intended way: one server on its own machine

It is intended that each server is on its own machine.

The IP address of the LoginServer is the IP address on the server line.
The IP address of channels is unused.

In LoginServer.ini, the IP address of the CacheServer is written. Only one CacheServer
is supported per server.


### The hacky way: changing the code so one machine can support multiple servers


- Neuz will always connect on port 28000 for LoginServer
- Database server will always listen on ports 7000, 24000 and 7200, so Core,
World and Login will connect on these
- Neuz will always connect to the cache whose port is fixed, 5400 by default.
It does not depend on the cluster/channel it connects to. Cache will always
listen on a fixed port, the 5400 by default.
- CoreServer will always listen on ports 34000, 34001 and 34002 for World,
Cache and Login.

The only processes that do not use fixed port, but ports depending on the key
is the WorldServer - Cache link. This is why you can run multiple channels on
the same VPS without issue.

A simple tip is to use ports that depends on the server id.


