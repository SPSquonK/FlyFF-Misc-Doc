# Insights


## Increasing the hp / damage / penya / ... limit is a lot of work


These limits are bound to the maximum value an int32 can support (~2.1 * 10^9)
so you need to change all these ints that store hp and damages to a larger
type, for example std::int64_t (aka long long, with a maximum value of
~9 * 10^18), or if you truly want no limit, change all these int to a
limitless number implementation (like this one for example :
https://github.com/faheel/BigInt ).

All the implicit conversion from int64 to int(32), or from a big int
implementation to int (if they exist in the chosen library) make the change
very error pone. You forget to replace one int to an int64? Boom, in some
weird conditions, you may deal negative damage, the damage of one spell may be
capped to 2M, or you may get instakilled by a dot skill, ...

If one really wants to use int64 for hp and damages, I would recommend using a
library like NamedType / StrongType
( https://github.com/joboccara/NamedType / https://github.com/rollbear/strong_type )
to avoid implicit conversion, and instead produce a compile error. That would
at least limit the number of bugs.

Alternatively, promote the "lose of data" warning to an error. But it won't
catch explicit casts.


## How to implement multiple CacheServers

- Add a mechanism to let LoginServer provide the right IP address
of the CacheServer depending on the channel
- In CoreServer, the key of a player is not only the serial, but now
the (cache dpid, serial) pair. Or the (channel id, serial) pair.



