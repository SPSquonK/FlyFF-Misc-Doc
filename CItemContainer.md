# CItemContainer


/!\ Information in this document needs to be checked

```cpp
class CItemContainer {
public:
	std::vector<DWORD> m_apIndex;
	std::vector<CItemElem> m_apItem;
	DWORD	m_dwIndexNum = 0;
	DWORD	m_dwItemMax = 0;

	assert(m_apIndex.size() == m_dwItemMax)
	assert(m_apItem.size() == m_dwItemMax)
};
```


An item container has two size:
- The total number of items
- The number of regular items

Given a `CItemElem * pItemElem`:
- `pItemElem == &m_apIndex[pItemElem->m_dwObjId]`
- `pItemElem == &m_apIndex[m_apIndex[pItemElem->m_dwObjIndex]]`

In all containers but the inventory:
- `pItemElem->m_dwObjId == pItemElem->m_dwObjIndex`
- `m_dwIndexNum == m_dwItemMax`

> (From here, information may be wrong)

`CItemContainer::GetAt` uses `pItemElem->m_dwObjId`.

Use
- `pItemElem->m_dwObjId` with `GetAt`
- `pItemElem->m_dwObjIndex` with `GetAtId`, `m_apIndex`, `Swap`, or `DoEquip` should be near.

## Practical guide

- Iterate on all items that can be used:

```cpp
	for (int i = 0; i < pUser->m_Inventory.GetMax(); ++i) {
		const CItemElem * pItemElem = pUser->m_Inventory.GetAtId(i);
		if (!pItemElem) continue;
		if (!IsUsableItem(pItemElem)) continue;
		if (pUser->m_Inventory.IsEquip(pItemElem->m_dwObjId)) continue;
		if (pUser->IsUsing(pItemElem)) continue;

    /* do things */
	}
```



## Improvements ideas

- If your implementation is using templates, and if you have 1 hour to lose, you can either remove the template or remove in the code the assumption that `T = CItemElem`.


- Implement a range that enables to loop on all `CItemElem` that can be used:

```cpp
for (CItemElem * pItemElem : pUser->GetAllModifiableInInventory()) {
  // do things
}
```


