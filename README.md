# FlyFF Misc Documentation

This repository is a compilation of things I wrote about FlyFF architecture.

The studied architecture is the V15 architecture.

- [Executables, links and cardinalities](Executables-Links-Cardinalities)
- [Reference sheets](Reference-sheets)
- [CItemContainer implementation and usage](CItemContainer)
- [Short notes](Short-notes.md)


## License

This repository has been written by SquonK ~~and other contributors~~.

Do not post the content of this repository on some random websites.

Instead:
- Post a link to this repository
- Eventually press the fork button
